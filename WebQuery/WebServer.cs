﻿using System;
using System.IO;
using System.Text;
using System.Net;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace WebQuery
{
    class WebServer
    {
        public static HttpListener listener;

        const string QueryImgSrc = "{query-img-src}";
        const string QueryImgName = "{query-img-name}";
        const string QueryResultsCount = "{query-results-count}";
        const string ResultsArray = "{results-array}";
        const string ResultImgSrc = "{result-img-src}";
        const string ResultImgNumber = "{result-img-number}";
        const string ResultImgName = "{result-img-name}";
        const string PrevQuery = "{prev-query}";
        const string NextQuery = "{next-query}";
        const string PrevDisabled = "{prev-disabled}";
        const string NextDisabled = "{next-disabled}";
        const string Author = "{author}";
        const string QueryResultsFileName = "{query-results-filename}";
        const string LastModifiedDate = "{last-modified-date}";
        const string TextDescription = "{text-description}";
        const string QueryDescriptor = "{query-descriptor}";

        private static Dictionary<string, FileSystemWatcher> authorResultWatchers = new System.Collections.Generic.Dictionary<string, FileSystemWatcher>();
        private static Dictionary<string, DateTime> authorResultDate = new Dictionary<string, DateTime>();
        private static Dictionary<string, string> authorResultFilename = new Dictionary<string, string>();

#if DEBUG

        public static string testFolder = @"T:\vis\img\image_test";
        public static string queryFolder = @"T:\vis\img\image_query";
        public static string testDescriptorFolder = @"T:\vis\img\text_test";
        public static string queryDescriptorFolder = @"T:\vis\img\text_query";

        public static string pageData = File.ReadAllText("X:\\web\\vis\\index.html");
        public static string cardData = File.ReadAllText("X:\\web\\vis\\result-card.html");
        public static string nothingData = File.ReadAllText("X:\\web\\vis\\index-nothing.html");

        public static string resultsRoot = @"T:\vis\img\results";
        public static string[] queryFiles = Directory.GetFiles(queryFolder, "*.jpg");

        public static object authorsResultsLock = new object();
        public static Dictionary<string, string[]> authorsResults = new Dictionary<string, string[]>();


#else

        public static string testFolder = @"image_test";
        public static string queryFolder = @"image_query";
        public static string testDescriptorFolder = @"text_test";
        public static string queryDescriptorFolder = @"text_query";

        public static string pageData = File.ReadAllText("index.html");
        public static string cardData = File.ReadAllText("result-card.html");
        public static string nothingData = File.ReadAllText("index-nothing.html");

        public static string resultsRoot = Path.Combine(Environment.CurrentDirectory, "results");

        public static string[] queryFiles = Directory.GetFiles(queryFolder, "*.jpg");

        public static object authorsResultsLock = new object();
        public static Dictionary<string, string[]> authorsResults = new Dictionary<string, string[]>();

#endif

        private static FileSystemWatcher rootWatcher = new FileSystemWatcher(resultsRoot);

        static WebServer()
        {
            string[] authors = (from string author in Directory.GetDirectories(resultsRoot) select Path.GetFileName(author)).ToArray();

            Array.Sort(queryFiles, (x, y) => String.Compare(x, y));

            // New folder created
            rootWatcher.Created += resultsFolder_Changed;
            rootWatcher.Changed += resultsFolder_Changed;

            foreach (var author in authors)
            {
                LoadAuthor(author);
            }
        }

        static void resultsFolder_Changed(object o, FileSystemEventArgs e)
        {
            // Skip if it's a file
            FileAttributes attr = File.GetAttributes(e.FullPath);
            if (!attr.HasFlag(FileAttributes.Directory))
                return;

            var di = new DirectoryInfo(e.FullPath);
            LoadAuthor(di.Name);
        }

        static void LoadAuthor(string author)
        {
            Console.WriteLine("Loading author: " + author);
            string defaultResultFile;
            string[] defaultResultFileLines;
            DateTime resultDate;

            try
            {
                var resultFiles = Directory.GetFiles(Path.Combine(resultsRoot, author));
                if (resultFiles.Length == 0)
                {
                    lock (authorsResultsLock)
                    {
                        authorsResults.Add(author, null);
                        authorResultFilename.Add(author, null);
                        authorResultDate.Add(author, DateTime.MinValue);

                        if (!authorResultWatchers.ContainsKey(author))
                        {
                            var watcher = new FileSystemWatcher(Path.Combine(resultsRoot, author), "*.txt");
                            watcher.NotifyFilter = NotifyFilters.LastWrite;
                            watcher.EnableRaisingEvents = true;
                            watcher.Created += Watcher_Changed;
                            watcher.Changed += Watcher_Changed;

                            Console.WriteLine("Folder empty, but still created a watcher at: " + Path.Combine(resultsRoot, author));

                            authorResultWatchers.Add(author, watcher);
                        }
                    }
                }

                defaultResultFile = resultFiles[0];
                defaultResultFileLines = File.ReadAllLines(defaultResultFile);
                resultDate = new FileInfo(defaultResultFile).LastWriteTime;
            }
            catch
            {
                return;
            }

            lock (authorsResultsLock)
            {
                if (!authorsResults.ContainsKey(author))
                    authorsResults.Add(author, defaultResultFileLines);
                else
                    authorsResults[author] = defaultResultFileLines;

                if (!authorResultDate.ContainsKey(author))
                    authorResultDate.Add(author, resultDate);
                else
                    authorResultDate[author] = resultDate;

                if (!authorResultFilename.ContainsKey(author))
                    authorResultFilename.Add(author, Path.GetFileName(defaultResultFile));
                else
                    authorResultFilename[author] = Path.GetFileName(defaultResultFile);

                if (!authorResultWatchers.ContainsKey(author))
                {
                    var watcher = new FileSystemWatcher(Path.Combine(resultsRoot, author), "*.txt");
                    watcher.NotifyFilter = NotifyFilters.LastWrite;
                    watcher.EnableRaisingEvents = true;
                    watcher.Created += Watcher_Changed;
                    watcher.Changed += Watcher_Changed;

                    Console.WriteLine("Created watcher at: " + Path.Combine(resultsRoot, author));

                    authorResultWatchers.Add(author, watcher);
                }
            }
        }

        static async Task<string[]> TryReloadFile(string filename)
        {
            int retries = 0;
            string[] results;
            while (retries < 10)
            {
                try
                {
                    results = await File.ReadAllLinesAsync(filename);
                    return results;
                }
                catch
                {
                    await Task.Delay(1000);
                    retries--;
                    continue;
                }
            }
            return null;
        }

        private static async void Watcher_Changed(object sender, FileSystemEventArgs e)
        {
            Console.WriteLine("File changed: " + e.FullPath);
            try
            {
                var fi = new FileInfo(e.FullPath);
                var author = fi.Directory.Name;

                Console.WriteLine("Author: " + author);

                if (!authorsResults.ContainsKey(author))
                    return;

                var contents = await TryReloadFile(e.FullPath);

                lock (authorsResultsLock)
                {
                    authorResultFilename[author] = fi.Name;
                    authorResultDate[author] = fi.LastWriteTime;
                    authorsResults[author] = contents;
                }
            }
            catch
            {
                return;
            }
        }

        public static async Task HandleIncomingConnections()
        {
            bool runServer = true;

            // While a user hasn't visited the `shutdown` url, keep on handling requests
            while (runServer)
            {
                //int stage = 0;
                try
                {

                    // Will wait here until we hear from a connection
                    HttpListenerContext ctx = await listener.GetContextAsync();
                    //stage++;
                    // Peel out the requests and response objects
                    HttpListenerRequest req = ctx.Request;
                    //stage++;
                    HttpListenerResponse resp = ctx.Response;
                    //stage++;

                    string requestPath = req.Url.AbsolutePath;
                    //stage++;                    

                    if (requestPath.StartsWith("/img/test/"))
                    {
                        await HandleImage(Path.Combine(testFolder, requestPath.Substring(10)), resp);
                        //stage++;
                    }
                    else if (requestPath.StartsWith("/img/query/"))
                    {
                        await HandleImage(Path.Combine(queryFolder, requestPath.Substring(11)), resp);
                        //stage++;
                    }
                    else
                    {
                        await HandleIndex(requestPath, resp);
                        //stage++;
                    }

                    resp.Close();
                    //stage++;
                }
                catch (HttpListenerException)
                {

                }
            }
        }

        private static void HandleRedirectFirstAuthor(HttpListenerResponse resp)
        {
            resp.StatusCode = (int)HttpStatusCode.Redirect;
            resp.RedirectLocation = $"/{authorsResults.First().Key}/0";
        }

        private static void HandleRedirectFirstPage(HttpListenerResponse resp, string author)
        {
            resp.StatusCode = (int)HttpStatusCode.Redirect;
            resp.RedirectLocation = $"/{author}/0";
        }

        private static void HandleRedirectLastPage(HttpListenerResponse resp, string author, int lastPage)
        {
            resp.StatusCode = (int)HttpStatusCode.Redirect;
            resp.RedirectLocation = $"/{author}/{lastPage.ToString()}";
        }

        private static async Task HandleIndex(string requestPath, HttpListenerResponse resp)
        {
            // Print out some info about the request
            Console.WriteLine("Request: " + requestPath);
            Console.WriteLine(requestPath);
            Console.WriteLine();

#if DEBUG
            pageData = File.ReadAllText("X:\\web\\vis\\index.html");
            cardData = File.ReadAllText("X:\\web\\vis\\result-card.html");
            nothingData = File.ReadAllText("X:\\web\\vis\\index-nothing.html");
#endif
            // expected: requestPath = /<author>/<query number>, e.g /hthieu/0 /hthieu/1 /hthieu/2 --> results[0], results[1], results[2]

            var requestPathParts = requestPath.Split('/', StringSplitOptions.RemoveEmptyEntries);

            if (requestPathParts.Length == 1 && authorsResults.ContainsKey(requestPathParts[0]) && authorsResults[requestPathParts[0]] == null)
            {
                await HandleNothingFound(resp, requestPathParts[0]);
                return;
            }

            if (requestPathParts.Length != 2)
            {
                HandleRedirectFirstAuthor(resp);
                return;
            }

            var selectedAuthor = requestPathParts[0];

            if (!authorsResults.ContainsKey(selectedAuthor))
            {
                HandleRedirectFirstAuthor(resp);
                return;
            }

            if (authorsResults[selectedAuthor] == null)
            {
                await HandleNothingFound(resp, selectedAuthor);
                return;
            }

            string[] selectedAuthorResults;
            string filename;
            DateTime date;

            lock (authorsResultsLock)
            {
                selectedAuthorResults = authorsResults[selectedAuthor];
                filename = authorResultFilename[selectedAuthor];
                date = authorResultDate[selectedAuthor];
            }

            if (!int.TryParse(requestPathParts[1], out int queryNumber))
            {
                HandleRedirectFirstPage(resp, selectedAuthor);
                return;
            }

            if (queryNumber >= selectedAuthorResults.Length)
            {
                HandleRedirectLastPage(resp, selectedAuthor, selectedAuthorResults.Length - 1);
                return;
            }

            string imgname = (queryNumber + 1).ToString().PadLeft(6, '0');
            string imgsrc = (queryNumber + 1).ToString().PadLeft(6, '0') + ".jpg";

            StringBuilder results = new StringBuilder();
            var cards = selectedAuthorResults[queryNumber].Split(' ');

            var cardsCount = Math.Min(100, cards.Length);

            for (var i = 0; i < cardsCount; i++)
            {
                var cardname = cards[i].PadLeft(6, '0');
                var cardimg = cardname + ".jpg";

                var descriptor = "(unknown)";

                try
                {
                    descriptor = File.ReadAllText(Path.Combine(testDescriptorFolder, cardname + ".txt"));
                }
                catch
                {

                }

                string card1 = cardData
                    .Replace(TextDescription, descriptor)
                    .Replace(ResultImgSrc, cardimg)
                    .Replace(ResultImgName, cardname)
                    .Replace(ResultImgNumber, i.ToString());
                results.Append(card1);
            }

            var query_descriptor = "(unknown)";

            try
            {
                query_descriptor = File.ReadAllText(Path.Combine(queryDescriptorFolder, imgname + ".txt"));
            }
            catch
            {

            }

            string currentPage = pageData
                .Replace(QueryDescriptor, query_descriptor)
                .Replace(Author, selectedAuthor)
                .Replace(QueryResultsFileName, filename)
                .Replace(LastModifiedDate, date.ToString("HH:mm:ss dd-MMM-yyyy"))
                .Replace(QueryImgSrc, imgsrc)
                .Replace(QueryImgName, imgname)
                .Replace(QueryResultsCount, cardsCount.ToString())
                .Replace(PrevQuery, (queryNumber == 0 ? "Nôôô hết rồi" : ((queryNumber - 1).ToString())))
                .Replace(PrevDisabled, (queryNumber == 0 ? "disabled" : ""))
                .Replace(NextQuery, (queryNumber == selectedAuthorResults.Length - 1 ? "Nôôô hết rồi" : ((queryNumber + 1).ToString())))
                .Replace(NextDisabled, (queryNumber == selectedAuthorResults.Length - 1 ? "disabled" : ""))
                .Replace(ResultsArray, results.ToString());

            byte[] data = Encoding.UTF8.GetBytes(currentPage);
            resp.ContentType = "text/html";
            resp.ContentEncoding = Encoding.UTF8;
            resp.ContentLength64 = data.LongLength;
            resp.StatusCode = (int)HttpStatusCode.OK;

            // Write out to the response stream (asynchronously), then close it
            await resp.OutputStream.WriteAsync(data, 0, data.Length);
        }

        private static async Task HandleNothingFound(HttpListenerResponse resp, string author)
        {
            string response = nothingData.Replace(Author, author);
            byte[] data = Encoding.UTF8.GetBytes(response);
            resp.ContentType = "text/html";
            resp.ContentEncoding = Encoding.UTF8;
            resp.ContentLength64 = data.LongLength;
            resp.StatusCode = (int)HttpStatusCode.OK;

            // Write out to the response stream (asynchronously), then close it
            await resp.OutputStream.WriteAsync(data, 0, data.Length);
        }

        private static async Task HandleImage(string filename, HttpListenerResponse resp)
        {
            int bufferSize = 1024 * 16;

            using (Stream input = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.Read, bufferSize, true))
            {
                resp.ContentLength64 = input.Length;

                byte[] buffer = new byte[bufferSize];
                int nbytes;

                while ((nbytes = await input.ReadAsync(buffer, 0, buffer.Length)) > 0)
                    await resp.OutputStream.WriteAsync(buffer, 0, nbytes);

                resp.ContentType = "image/jpeg";
                resp.StatusCode = (int)HttpStatusCode.OK;
                resp.OutputStream.Flush();
            }
        }
    }
}
