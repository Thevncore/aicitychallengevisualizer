﻿using System;
using System.IO;
using System.Text;
using System.Net;
using System.Threading.Tasks;
using static WebQuery.WebServer;

namespace WebQuery
{
    class Program
    {
        static void Main(string[] args)
        {
            string url = "http://*:8000";

            listener = new HttpListener();

#if DEBUG
            listener.Prefixes.Add("http://localhost:8000/");
#else
            listener.Prefixes.Add("http://*:20380/");
#endif
            listener.Start();
            Console.WriteLine("Listening for connections.");

            // Handle requests
            Task listenTask = HandleIncomingConnections();
            listenTask.GetAwaiter().GetResult();

            // Close the listener
            listener.Close();
        }

    }
}
